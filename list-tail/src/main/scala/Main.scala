import List._

object Main extends App {

  val list = List(3, 5, 7)
  val t = tail(list)

  println("t = " + t)
}
